<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>



<article <?php post_class(); ?>>
	<div class="media">
		<div class="pull-left">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail('blog-thumb'); ?>
			<?php endif; ?> 
		</div>
	</div>
</article>